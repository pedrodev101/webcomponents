const fs = require("fs-extra");
const concat = require("concat");

(async function build() {
  const files = [
    "./dist/form-cadastro-gestante/runtime.js",
    "./dist/form-cadastro-gestante/polyfills.js",
    "./dist/form-cadastro-gestante/main.js",
  ];

  await fs.ensureDir("elements");
  await concat(files, "elements/form-gestante.js");
})();
